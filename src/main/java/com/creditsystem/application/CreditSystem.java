package com.creditsystem.application;

import com.creditsystem.entity.Client;
import com.creditsystem.entity.Credit;

import java.util.List;

public class CreditSystem {

    private int sum;

    public void getCreditToClient(Client client, int sum, int time, double percent){
        Credit credit = new Credit(sum, time, percent);
        client.addCredit(credit);
    }

    public int repayCredit(Credit credit, int enterSum){
        int amount = sum - enterSum;
        System.out.println("Вы внесли " + enterSum + " руб, оставшаяся сумма - " + amount);
        return amount;
    }

    public Integer sumOfCredits(Client client){
        Integer sum = 0;
        for(Credit credit : client.getCredits()){
            sum += credit.getSum();
        }
        return sum;
    }

}
