package com.creditsystem;

import com.creditsystem.application.CreditSystem;
import com.creditsystem.entity.Client;
import com.creditsystem.entity.Credit;

import java.util.*;

public class Main {

    static CreditSystem CREDIT_SYSTEM = new CreditSystem();

    public static void main(String[] args) {
        Client client1 = new Client(2, "Алексей", "Петров", 24, "мужской", "Самара", "мобильный");
        Client client2 = new Client(1, "Алексей", "Гаврилов", 24, "мужской", "Самара", "мобильный");
        Client client3 = new Client(3, "Ярослав", "Подольский", 24, "мужской", "Самара", "мобильный");
        Client client4 = new Client(5, "Яков", "Митрополит", 24, "мужской", "Самара", "мобильный");
        Client client5 = new Client(4, "Михаил", "Кротов", 24, "мужской", "Самара", "мобильный");

        List<Client> clients = Arrays.asList(client1, client2, client3, client4, client5);

        CREDIT_SYSTEM.getCreditToClient(client1, 35000, 12, 2);
        CREDIT_SYSTEM.getCreditToClient(client1, 55000, 12, 2);
        CREDIT_SYSTEM.getCreditToClient(client1, 55000, 12, 2);

        CREDIT_SYSTEM.getCreditToClient(client2, 5000, 12, 2);
        CREDIT_SYSTEM.getCreditToClient(client2, 15000, 12, 2);
        CREDIT_SYSTEM.getCreditToClient(client2, 25000, 12, 2);

        CREDIT_SYSTEM.getCreditToClient(client3, 25000, 12, 2);
        CREDIT_SYSTEM.getCreditToClient(client3, 35000, 12, 2);
        CREDIT_SYSTEM.getCreditToClient(client3, 45000, 12, 2);

        CREDIT_SYSTEM.getCreditToClient(client3, 5000, 12, 2);
        CREDIT_SYSTEM.getCreditToClient(client3, 5000, 12, 2);
        CREDIT_SYSTEM.getCreditToClient(client3, 5000, 12, 2);

        CREDIT_SYSTEM.getCreditToClient(client3, 25000, 12, 2);
        CREDIT_SYSTEM.getCreditToClient(client3, 35000, 12, 2);
        CREDIT_SYSTEM.getCreditToClient(client3, 245000, 12, 2);

        System.out.println(CREDIT_SYSTEM.sumOfCredits(client1));
        System.out.println(CREDIT_SYSTEM.sumOfCredits(client2));
        System.out.println(CREDIT_SYSTEM.sumOfCredits(client3));

        clients.sort(Comparator.comparing(Client::getFirstName));
        System.out.println(clients);

        List<Integer> sumOfCreditsList = new ArrayList<>();
        sumOfCreditsList.add(CREDIT_SYSTEM.sumOfCredits(client1));
        sumOfCreditsList.add(CREDIT_SYSTEM.sumOfCredits(client2));
        sumOfCreditsList.add(CREDIT_SYSTEM.sumOfCredits(client3));
        sumOfCreditsList.sort(Comparator.naturalOrder());
        System.out.println(sumOfCreditsList);

        clients.sort(Comparator.comparing(Client::getFirstName).thenComparing(o -> CREDIT_SYSTEM.sumOfCredits(o)));
        System.out.println(clients);
    }
}
